const express = require('express');
const bodyParser = require('body-parser');

const product = require('./routes/product.route'); // Imports routes for the products
const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
 
var options = { keepAlive: 300000, connectTimeoutMS: 30000, useNewUrlParser: true};   
 
var mongodbUri = 'mongodb://localhost/productstutorial';
 
mongoose.connect(mongodbUri, options);
mongoose.set('useFindAndModify', false);
var conn = mongoose.connection;             
 
conn.on('error', console.error.bind(console, 'connection error:'));
 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/products', product);

let port = 3000;

app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});